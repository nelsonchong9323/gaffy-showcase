$(function () {
    var scroll_start = 0;

    $('[data-toggle="tooltip"]').tooltip();

    onScrollColorChange();
    smoothScrolling();
    onResize();

    $(window).on('resize', function () {
        onResize();
    });
});

function onResize() {

    $(document).on('scroll', function () {
        onScrollColorChange();
    });


    if ($(window).width() < 992) {
        $('#pnlLoginRegister').addClass('hide');
    } else {
        $('#pnlLoginRegister').removeClass('hide');
    }
}

function onScrollColorChange() {
    var startchange1 = $('#secLanding');
    var startchange2 = $('#secChoose');
    var startchange3 = $('#secPricing');
    var offset1 = startchange1.offset();
    var offset2 = startchange2.offset();
    var offset3 = startchange3.offset();

    scroll_start = $(document).scrollTop();
    if (scroll_start < offset2.top - 50) {
        $('#navMain').removeClass('sec2 sec3').addClass('sec1');
        $('.back-to-top.fade').removeClass('in');
    }

    if (scroll_start > offset2.top - 50) {
        $('#navMain').removeClass('sec1 sec3').addClass('sec2');
        $('.back-to-top.fade').addClass('in');
    }

    if (scroll_start > offset3.top - 50) {
        $('#navMain').removeClass('sec1 sec2').addClass('sec3');
        $('.back-to-top.fade').addClass('in');
    }
}

function smoothScrolling() {
    $('a[href^="#"]:not(.exempted)').on('click', function (event) {
        var target = $(this.getAttribute('href'));
        if (target.length) {
            event.preventDefault();
            $('html, body').stop().animate({
                scrollTop: target.offset().top
            }, 900);
            $('.tooltip').tooltip('hide');
        }
    });
}
