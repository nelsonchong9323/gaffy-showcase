$(function () {
    togglePanelLoginRegister();
    hideTooltipOnBlur();
});


function togglePanelLoginRegister() {
    $('.btn-register').click(function () {
        $('#pnlLoginRegister').removeClass('hide');

        $('#pnlRegister.hide, #pnlLogin:not(.hide)').toggleClass('hide');

        $('li a.btn-register:not(.active), li a.btn-login.active').toggleClass('active');
    });

    $('.btn-login').click(function () {
        $('#pnlLoginRegister').removeClass('hide');

        $('#pnlLogin.hide, #pnlRegister:not(.hide)').toggleClass('hide');

        $('li a.btn-login:not(.active), li a.btn-register.active').toggleClass('active');
    });

    $('.btn-dismiss').click(function () {
        $('#pnlLoginRegister').addClass('hide');
    });
}

function hideTooltipOnBlur() {
    $('[data-toggle="tooltip"]').on('blur', function () {
        $('.tooltip').tooltip('hide');
    });
}
