$(function () {
    stepButtonStatus();

    $('#inputUpload').click(function () {
        customFileInput();
    }).change(function () {
        customFileInput();
    });
});

function stepButtonStatus() {
    $('#ulSteps li.active ~ li').removeClass('done').find('a').removeAttr('data-toggle');

    $(".btn[href*='step']").click(function () {
        var href = $(this).attr('href');

        $('#ulSteps li').each(function () {
            $(this).find('a[href="' + href + '"]').attr('data-toggle', 'tab').closest('li').addClass('active done').siblings('li.active').removeClass('active');

            $('#ulSteps li.active').prev().addClass('done');
            $('#ulSteps li.active ~ li:not(.done)').find('a').removeAttr('data-toggle');
        });
    });
}

function customFileInput() {
    var file = $('#inputUpload')[0].files[0]['name'];

    if (file) {
        $('.uploaded-file-name').text(file);
    }
}
