$(function () {
    toggleIcon();
    modalContentSwitching();
    stepButtonStatus();
    
    $('#inputUpload').click(function(){
        customFileInput();
    }).change(function(){
        customFileInput();
    });
});

function toggleIcon() {
    var target = $('.link-title');

    target.on('click', function () {
        var panel = $(this).siblings('.panel-collapse');
        return panel.hasClass('in') ? ($(this).find('.fa-plus-square.hide, .fa-minus-square:not(.hide)').toggleClass('hide')) : ($(this).find('.fa-plus-square:not(.hide), .fa-minus-square.hide').toggleClass('hide'));
    });
}

function modalContentSwitching() {
    var modal = $('#myModal');
    
    modal.on('show.bs.modal', function (e) {
        var invoker = $(e.relatedTarget);
        if(invoker.hasClass('btn-process')){
            modal.find('.step1-modal-content.hide, .step2-modal-content:not(.hide)').toggleClass('hide');
            modal.find('.modal-header').removeClass('hide');
            modal.find('.modal-footer').find('.btn-next').attr('href','#step2');
        } else if (invoker.hasClass('btn-validate')){
            modal.find('.step1-modal-content:not(.hide), .step2-modal-content.hide').toggleClass('hide');
            modal.find('.modal-header').addClass('hide');
            modal.find('.modal-footer').find('.btn-next').attr('href','#step3');
        }
    });
    
    $('.btn-prev, .btn-next').click(function(){
        modal.modal('hide');
    });
}

function stepButtonStatus() {
    $('#ulSteps li.active ~ li').removeClass('done').find('a').removeAttr('data-toggle');
    
    $(".btn[href*='step']").click(function(){
        var href = $(this).attr('href');
        
        $('#ulSteps li').each(function(){
            $(this).find('a[href="'+href+'"]').attr('data-toggle','tab').closest('li').addClass('active done').siblings('li.active').removeClass('active');
        
            $('#ulSteps li.active').prev().addClass('done');
            $('#ulSteps li.active ~ li:not(.done)').find('a').removeAttr('data-toggle');
        }); 
    });
}

function customFileInput() {
    var file = $('#inputUpload')[0].files[0]['name'];
    
    if (file){
        $('.uploaded-file-name').text(file);
    }
}
